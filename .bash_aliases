#Les ALIAS doivent etre rédigé sous la forme :
#alias nom_de_votre_alias="commande de votre alias"

###APT###
alias install='sudo apt install'
alias uninstall='sudo apt autoremove --purge'
alias update='sudo apt update && apt list --upgradable'
alias upgrade='sudo apt update && sudo apt upgrade -y'
alias aptlist='apt list --manual-installed '

#########
alias shutdown='sudo shutdown now'
alias sshpi='ssh pi@192.168.1.31'

alias pico2wave='pico2wave -l fr-FR -w test.wav '
alias banner='figlet'

alias untar='tar -zxvf'
alias ping='ping -c 5'
alias tree="find . | sed 's/[^/]*\//|   /g;s/| *\([^| ]\)/+--- \1/'"
alias ln='ln -sv'
alias ps='watch -d -n0.5 ps u'

#Refresh the terminal bash
alias bash='bash --login'
# Always copy contents of directories (r)ecursively and explain (v) what was done
alias cp='cp -rv'
# Explain (v) what was done when moving a file
#alias mv='mv -v'
# Create any non-existent (p)arent directories and explain (v) what was done
#alias mkdir='mkdir -pv'
# Always try to (c)ontinue getting a partially-downloaded file
alias wget='wget -c'

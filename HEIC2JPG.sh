#!/bin/bash
#Init Color pour echo
NC='\033[0m'              # No Color
# Regular Colors
Red='\033[1;31m'          # Red
Green='\033[1;32m'        # Green
White='\033[4;37m'        # White

#sudo add-apt-repository ppa:strukturag/libheif
#sudo apt-get install libheif-examples
#sudo apt-get update

nbheic=$(ls -Al | find *.heic | wc -l);
if [ $nbheic == 0 ]; #Si je n ai pas de .heic dans mon repertoire
	then 
	echo -e "${Red}Il n'y a aucun .heic dans ce repertoire courant.${NC}"
else
	for f in *.heic
	do
		date=$(echo $f | cut -d"h" -f1);
		Name=$date"jpg";
		echo "$f ==> $Name"
		heif-convert "$f" "$Name";
		[ -d Save ] || mkdir './Save' && echo "Creat Save Repository";
		mv "$f" "./Save/$f"
	done
	echo "${White}$nbheic fichiers .heic ont étaient converti dans le dossier $path${NC}";
fi

#!/bin/bash
#Init Color pour echo
NC='\033[0m'              # No Color
# Regular Colors
Red='\033[1;31m'          # Red
Green='\033[1;32m'        # Green
White='\033[4;37m'        # White


echo -e "Ce Script renome les .JPG du dossier en fonction de la date."
read -p "Press any key to continue..." -n1 -s
echo -e ""

nbJPG=$(ls -Al | find *.png | wc -l);
path=$(pwd);

if [ $nbJPG == 0 ]; #Si je n ai pas de .JPG dans mon repertoire
	then 
	echo -e "${Red}Il n'y a aucun .JPG dans ce repertoire courant.${NC}"
else
	echo "il y a $nbJPG fichiers .jpg du repertoire : $path";
	mkdir -p ./Traité;
	mkdir -p ./Error;
	mkdir -p ./Done;
	for photo in *.png;do 
		echo -e "\n${White}Traitement du fichier $photo${NC}";
		jhead $photo >/dev/null 2>&1;
		if [ $? -ne 0 ]
			then
			echo -e "${Red}La commande jhead ne fonctionne pas sur ce fichier.\nsudo apt install jhead${NC}";
			mv "$photo" "./Error";
		else
			jhead $photo |grep 'File date' >/dev/null 2>&1;
			if [ $? -eq 0 ];
				then 
				Annee=$(jhead $photo|grep 'File date' | cut -d":" -f2 | cut -d" " -f2);
				Mois=$(jhead $photo|grep 'File date' | cut -d":" -f3);
				Jour=$(jhead $photo|grep 'File date' | cut -d":" -f4 | cut -d" " -f1);
				Heure=$(jhead $photo|grep 'File date' | cut -d":" -f4 | cut -d" " -f2); 
				Min=$(jhead $photo|grep 'File date' | cut -d":" -f5);
				Sec=$(jhead $photo|grep 'File date' | cut -d":" -f6);
				S="-";
				P=".";
				Name=$Annee$S$Mois$S$Jour" "$Heure$P$Min$P$Sec;
				#echo -e "Date : ${Green}$Name${NC}";
				cp "$photo" "./Done/$Name.png";
				mv "$photo" "./Traité";
				echo -e "${Green}$photo ==> $Name${NC}";
				
			else	
				mv "$photo" "./Error";
				echo -e "${Red}Error with $photo${NC}"
			fi
		fi
	done
	echo -e "${White}$nbJPG fichiers jpg ont étaient traité dans le dossier $path${NC}";
fi

#!/bin/bash
#Init Color pour echo
NC='\033[0m'              # No Color
# Regular Colors
Red='\033[1;31m'          # Red
Green='\033[1;32m'        # Green

echo "This Script will install 'sysfsutils' and configure it, witch allow your to connect Xbox One Bluetooth controller"
read -p "Press any key to continue..." -n1 -s
echo;
apt install sysfsutils;
if [ $? -ne 0 ]
	then
	cat > /etc/sysfs.conf << eof
	/module/bluetooth/parameters/disable_ertm=1
	eof
	echo -e "${Green}You can now connect your controller.${Red}";
	return 0;
else
	echo -e "${Red}Sysfsutils failed to install.${Red}";
	return 1;
if

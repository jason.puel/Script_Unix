#!/bin/bash
#Init Color pour echo
NC='\033[0m'              # No Color
# Regular Colors
Red='\033[1;31m'          # Red
Green='\033[1;32m'        # Green
White='\033[4;37m'        # White


echo "Ce Script renome les .MOV du dossier en fonction de la date."
read -p "Press any key to continue..." -n1 -s

nbmov=$(ls -Al | find *.mov | wc -l);
path=$(pwd);

if [ $nbmov == 0 ]; #Si je n ai pas de .MOV dans mon repertoire
	then 
	echo -e "${Red}Il n'y a aucun .mov dans ce repertoire courant.${NC}"
else
	echo "il y a $nbmov fichiers jpeg du repertoire : $path"
	
	for movie in *.mov;do 
		echo -e "\n${White}Traitement du fichier $movie${NC}";
		
		mediainfo "$movie" >/dev/null 2>&1;
		if [ $? -ne 0 ]
			then
			echo -e "${Red}La commande MEDIAINFO ne fonctionne pas sur ce fichier."
			echo -e "sudo apt  install mediainfo${NC}";
		else
			Date=$(mediainfo "$movie" | grep "Encoded date" | sed -n 1p | cut -d"C" -f2);
			Jour=$( echo $Date | cut -d" " -f1);
			heure=$( echo $Date | cut -d" " -f2 | cut -d":" -f1);
			min=$( echo $Date | cut -d" " -f2 | cut -d":" -f2);
			sec=$( echo $Date | cut -d" " -f2 | cut -d":" -f3);
			Name=$Jour" "$heure"."$min"."$sec;
			
			if [ -f "./$Name.mov" ]; then
				[ -d "./Copie" ] || mkdir "./Copie";
				mv "$Name.mov" "./Copie";
			    cp "$movie" "./Copie/$Name _2.mov";
			    echo -e "${Green}$movie ==> $Name.mov ${Yellow}Possible Copie${NC}";
			else
				[ -d "./Done" ] || mkdir "./Done";
				[ -d "./Traité" ] || mkdir "./Traité";
				cp "$movie" "./Done/$Name.mov";
				mv "$movie" "./Traité";
				echo -e "${Green}$movie ==> $Name.mov${NC}";
			fi
		fi
	done
	echo -e "${White}$nbmov fichiers .mov ont étaient traité dans le dossier $path${NC}";
fi

#!/usr/bin/bash
#Init Color pour echo
NC='\033[0m'              # No Color
# Regular Colors
Red='\033[1;31m'          # Red
Green='\033[1;32m'        # Green
White='\033[4;37m'        # White

echo "Ce Script va créer ~/.config/autostart/²tigntvnc.desktop. "
read -p "Press any key to continue..." -n1 -s

[ -d "~/.config/autostartS" ] || mkdir ~/.config/autostart
touch ~/.config/autostart/tigntvnc.desktop

cat > ~/.config/autostart/tigntvnc.desktop << eof
#!/bin/sh
[Desktop Entry]
Type=Application
Name=tightVNC
Exec=vncserver :1
StartupNotify=false
eof

if [ $? -ne 0 ]
	then
	echo "Weel Done"
else
	echo "Retry"
fi

#!/usr/bin/bash
#Init Color pour echo
NC='\033[0m'              # No Color
# Regular Colors
Red='\033[1;31m'          # Red
Green='\033[1;32m'        # Green
White='\033[4;37m'        # White

echo "Ce Script va trier les .MP3 en fonction des nom d'artists. "
read -p "Press any key to continue..." -n1 -s

nbmp3=$(ls -Al | find *.mp3 | wc -l);
path=$(pwd);

if [ $nbmp3 == 0 ]; #Si je n ai pas de .MP3 dans mon repertoire
	then 
	echo -e "${Red}Il n'y a aucun .MP3 dans ce repertoire courant.${NC}"
else
	echo "il y a $nbmov fichiers jpeg du repertoire : $path"
	for music in *.mp3;do 
		echo -e "\n${White}Traitement du fichier $music${NC}";
		mediainfo "$music" >/dev/null 2>&1;
		if [ $? -ne 0 ]
			then
			echo -e "${Red}La commande MEDIAINFO ne fonctionne pas sur ce fichier.";
			echo -e "sudo apt  install mediainfo${NC}";
		else
			Performer=$(mediainfo "$music" | grep "Performer   " | sed -n 1p | cut -d":" -f2);
			Performer=${Performer:1};
			if [ $? -ne 0 ];then
				echo -e "Unknown Artist";
				[ -d "./UnknownArtist" ] || mkdir "./UnknownArtist"; 
				mv "$music" "./UnknownArtist";
			else
				Album=$(mediainfo "$music" | grep "Album   " | sed -n 1p | cut -d":" -f2);
				Album=${Album:1};
				if [ $? -ne 0 ];then
					echo -e "$Performer: Unknown Album";
					[ -d "./$Performer" ] || mkdir -p "./$Performer"; 
					mv "$music" "./$Performer";
				else
					echo -e "$Performer: $Album";
					[ -d "./$Performer/$Album" ] || mkdir -p "./$Performer/$Album"; 
					mv "$music" "./$Performer/$Album";
				fi
			fi
		fi
	done
fi

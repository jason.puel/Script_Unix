#!/usr/bin/bash

echo 'Nom du raccourci :'
read name
touch $HOME/.local/share/applications/$name.desktop
echo 'Commande à executer :'
read Comm
echo 'Icon à afficher :'
read Icon

if [ -z "$Icon" ] #si pas d'image dispo
	then 
	echo 'Icon par default'
	Icon="$HOME/.local/share/icons/custom/Blocknote.png"
else
	#copier de l'icon
	nameICO=${data##*/}
	cp $Icon "$HOME/.local/icons/custom/$nameICO"
	Icon="$HOME/.local/icons/custom/$nameICO"
fi

cat > ~/.local/share/applications/$name.desktop << eof
#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=$name
GenericName=$name
Exec=$Comm
Icon=$Icon
StartupNotify=false
Categories=Application
#NoDisplay=true 
eof
chmod 751 ~/.local/share/applications/$name.desktop
ln -s ~/.local/share/applications/$name.desktop ~/Bureau/
cp ~/.local/share/applications/$name.desktop ~/Bureau/$name.desktop
